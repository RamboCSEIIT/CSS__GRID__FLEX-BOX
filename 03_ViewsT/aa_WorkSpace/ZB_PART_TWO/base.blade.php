<!DOCTYPE html>
<html>


<head>
    <meta charset=utf-8>
    <meta http-equiv=X-UA-Compatible content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name=description content="">
    <meta name=author content="">
    <link rel="shortcut icon" type=image/png href="03_IMAGES/favicon.png">
    <title>@yield('title')</title>
    {{-- <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">--}}
    <style>  @include('01_CSS.zb_part_two') </style>



</head>


<body style="background-color: green">

{{--
<!-- Source -->
<input required class="input-email" type="email" id="inputEmail" name="email">

<input required class="input-email" type="email" id="inputEmail" name="email"  >
--}}

@yield('content')
<script>
    @include('00_SCRIPTS.zb_part_two')
</script>
<script src="02_SCRIPTS/jquery-0.js"></script>
<script src="02_SCRIPTS/jquery-1.js"></script>
</body>

</html>