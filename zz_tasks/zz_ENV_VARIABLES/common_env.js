 

module.exports = 
{
  
  test_env: function () 
  {
      if(global.printMsg)
       console.log(process.env.NODE_ENV + ":: Test ENV---------------------------");
  }
  ,    
  test_env_dev: function () 
  {
    if(global.printMsg)
      console.log( process.env.NODE_ENV+"::development---------------------------");
  }
  ,
  test_env_prod: function () 
  {    
    if(global.printMsg)
      console.log(process.env.NODE_ENV + "::production---------------------------");
  }  
  ,
  set_mode_dev_env : function () 
  {
    
    return process.env.NODE_ENV =  'development';
  }
  ,
  set_mode_deploy_env: function () 
  {   
   
    return process.env.NODE_ENV =  'production';
  }
  
   
  
  
};