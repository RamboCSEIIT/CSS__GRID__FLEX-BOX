        
        
var gulp = require('gulp');
var sass = require('gulp-sass');
var gulpif = require('gulp-if');

var notify = require('gulp-notify');

var cssbeautify = require('gulp-cssbeautify');
var compass = require('compass-importer');
var concat = require('gulp-concat');
const stripCssComments = require('gulp-strip-css-comments');
const cssDeclarationSorter = require('css-declaration-sorter');


var postcss      = require('gulp-postcss');
var sourcemaps   = require('gulp-sourcemaps');
var autoprefixer = require('autoprefixer');
let cleanCSS = require('gulp-clean-css');
var COMPILED_CSS_PATH                              = '05_COMPILED_CSS';
var SASS_OUT_PATH                                = 'aa_template/resources/views/01_CSS';
var uglifycss = require('gulp-uglifycss');
var gulpMinifyCssNames = require('gulp-minify-css-names');
module.exports =
        {


            auto_prefix: function () {





                    return gulp.src(COMPILED_CSS_PATH+'/*.php')
                        .pipe(stripCssComments())
                        .pipe(postcss([ autoprefixer({ browsers: ['last 10 versions'],
                            cascade: false,
                            grid: true }),cssDeclarationSorter() ]))






                        .pipe(gulpif(process.env.NODE_ENV === 'development', cssbeautify({
                            indent: '      ',
                            openbrace: 'separate-line',
                            autosemicolon: false,
                        }),uglifycss({
                            "maxLineLen": 0,
                            "uglyComments": true
                        })))



                        .pipe(gulp.dest(SASS_OUT_PATH));


            }



            
  


        };




        
global.SCSS = function (options)
{


    return gulp.src( options.SASS_IN_PATH )
                
                
                      
                        .pipe(gulpif(process.env.NODE_ENV === 'development', sourcemaps.write()))
                        .pipe(sass({importer: compass}).on('error', function (err){
                            notify().write(err);
                            this.emit('end');
                        }))
                        .pipe(concat(options.SASS_OUT_FILE_NAME))
                        .pipe(gulp.dest(COMPILED_CSS_PATH));
                        /* .pipe(gulp.dest(options.SASS_OUT_PATH)); */

};