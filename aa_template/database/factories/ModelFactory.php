<?php

use Illuminate\Support\Facades\Hash;

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->firstName,
        'lastname' => $faker->lastName,
        'email' => $faker->safeEmail,
        'password' => Hash::make("bbbbbb"),
        'remember_token' => str_random(10),
    ];
});

 

 

$factory->define(App\Admin::class, function (Faker\Generator $faker) {
    return [

        'name' => $faker->firstName,
        'email' =>$faker->freeEmail,
        'password' => Hash::make('aaaaaa'),
        'remember_token' => str_random(10),

    ];
});

 $factory->define(App\Role::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->jobTitle,
    ];
});

$factory->define(App\Role_admin::class, function (Faker\Generator $faker) {
    return [
        'role_id' => $faker->randomNumber(),
        'admin_id' => $faker->randomNumber(),
    ];
});

$factory->define(App\Category::class, function (Faker\Generator $faker) {
    return [
    ];
});

$factory->define(App\Models\AB_ADMIN_CATEGORY\Ab_categorie::class, function (Faker\Generator $faker) {
    return [
    ];
});

