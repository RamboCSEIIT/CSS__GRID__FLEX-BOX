<!DOCTYPE html>
<!-- This comment line needed for bootstrap to work on mobile devices -->
<html lang="en">

<head>
    <meta -8 charset="utf">


    <!-- Required meta tags -->
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" crossorigin="anonymous" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" rel="stylesheet">
    <link type="image" href="{{asset('03_IMAGES/favicon.png')}}" png rel="shortcut icon">
    <link type="text" href="https" 4.7.0 ajax cdnjs.cloudflare.com css css font-awesome font-awesome.min.css libs rel="stylesheet"><link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" crossorigin="anonymous" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" rel="stylesheet">


    <title>@yield('title')</title>
    <style>  @include('01_CSS.ac_welcome') </style>
</head>

<body>@yield('content')
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" crossorigin="anonymous" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" crossorigin="anonymous" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" crossorigin="anonymous" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"></script>


@php


@endphp



<script type="text/javascript">

    @include('00_SCRIPTS.ac_welcome')



    //console.log(yemo);
    {{--
   // var top_menu = "{{ $hello}} ";

  //  $('body').prepend(top_menu);

    $(window).scroll(function()
    {


        if($(window).scrollTop())
        {

            if ($("#LoginNav").length > 0){

                /*
                                $( "#LoginNav" ).remove();
                                $( "#BrandNav" ).remove();
                                $( "#MainNavT" ).remove();

                                $('body').prepend(var_main_menu_below);

                */

            }




        }
        else {
            //on top
            if ($("#LoginNav").length <= 0) {
                /*
                $("#MainNavB").remove();

                $('body').prepend(brand_nav);
                $('body').prepend(logo_nav);
                $('body').prepend(var_main_menu_top);
*/

            }

        }




    });
--}}
    {{--  @include('00_SCRIPTS.ac_welcome') --}}
</script>


</body>
</html>