/*
function supports_html5_storage(){
    try {
        return 'localStorage' in window && window['localStorage'] !== null;
    } catch(e) {
        return false;
    }
}
function supports_webworker() {
    if (typeof(Worker) !== "undefined") {
        return true;
    }
    else{
        return false;
    }
}
*/
 function WebWorker_Start() {
/*
     if('serviceWorker' in navigator)
     {
         navigator.serviceWorker
             .register('/aa_test_worker.js')
             .then(function() { console.log("Service Worker Registered"); })
             .catch(function (err) {
                 console.error(err); // the Service Worker didn't install correctly
             });;
     }
*/
}
const posts = [
    {title : 'Post One',body: 'This is Post One'},
    {title : 'Post Two',body: 'This is Post Two'}
];
function getPosts()
{
    setTimeout(function () {
        var OutPut = ' ';
        posts.forEach(function (post,index) {
            OutPut += '<li>'+post.title+'</li>';
        });
        document.body.innerHTML=OutPut;
    },3000);
}
function createPost(post)
{
    return new Promise(function (resolve, reject) {
        posts.push(post);
        var error=false;
        if(!error){
              resolve();
        }
        else
        {
              reject('error something wrong');
        }
    },3000);
}
const  Promise1 = Promise.resolve("Hello world");
const  Promise2 = 10;
const  Promise3 = new Promise(function (resolve, reject) {
    setTimeout(resolve,3000,"Hello");
});
const Promise4 =fetch('https://jsonplaceholder.typicode.com/todos/1')
    .then(response => response.json());
Promise.all([Promise1,Promise2,Promise3,Promise4]).then(function (values) {
    console.log(values);
});
function fn()
{
    // "On document ready" commands:
    console.log(document.readyState);
   // WebWorker_Start();
  //  getPosts();
    /*
    createPost({title : 'Post Three',body: 'This is Post Three'},getPosts);*/
/*
    createPost({title : 'Post Three',body: 'This is Post Three'})
        .then(getPosts)
        .catch(function (error) {
            console.log(error);
        });
*/
async function init ()
{
       await  createPost({title : 'Post Three',body: 'This is Post Three'});
       getPosts();
}
    init();
}
if(document.readyState != 'loading')
{
    fn();
}
else
{
    document.addEventListener('DOMContentLoaded', fn);
}
/*https://developer.mozilla.org/en-US/docs/Web/API/Document/readyState */