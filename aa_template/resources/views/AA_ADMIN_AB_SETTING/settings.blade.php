@extends('adminLayout.admin_design')

@section('content')


    <div id="content">
        <div id="content-header">
            <div id="breadcrumb"> <a class="tip-bottom" href="settings.html" title="Go to Home"><i class="icon-home"></i> Home</a> <a href="#">Form elements</a> <a class="current" href="#">Validation</a> </div>
            <h1>Form validation</h1>
        </div>
        <div class="container-fluid"><hr>
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                            <h5>Form validation</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <form class="form-horizontal" id="basic_validate" name="basic_validate" action="#" method="post" novalidate="novalidate">
                                <div class="control-group">
                                    <label class="control-label">Your Name</label>
                                    <div class="controls">
                                        <input id="required" name="required" type="text">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Your Email</label>
                                    <div class="controls">
                                        <input id="email" name="email" type="text">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Date (only Number)</label>
                                    <div class="controls">
                                        <input id="date" name="date" type="text">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">URL (Start with http://)</label>
                                    <div class="controls">
                                        <input id="url" name="url" type="text">
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <input class="btn btn-success" type="submit" value="Validate">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <div class="widget-box">
                        <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                            <h5>Numeric validation</h5>
                        </div>
                        <div class="widget-content nopadding">
                            <form class="form-horizontal" id="number_validate" name="number_validate" action="#" method="post" novalidate="novalidate">
                                <div class="control-group">
                                    <label class="control-label">Minimal Salary</label>
                                    <div class="controls">
                                        <input id="min" name="min" type="text">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Maximum Salary</label>
                                    <div class="controls">
                                        <input id="max" name="max" type="text">
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label">Only digit</label>
                                    <div class="controls">
                                        <input id="number" name="number" type="text">
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <input class="btn btn-success" type="submit" value="Validate">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span12">
                        <div class="widget-box">
                            <div class="widget-title"> <span class="icon"> <i class="icon-info-sign"></i> </span>
                                <h5>Security validation</h5>
                            </div>
                            <div class="widget-content nopadding">
                                <form class="form-horizontal" id="password_validate" name="password_validate" action="#" method="post" novalidate="novalidate">
                                    <div class="control-group">
                                        <label class="control-label">Password</label>
                                        <div class="controls">
                                            <input id="pwd" name="pwd" type="password">
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Confirm password</label>
                                        <div class="controls">
                                            <input id="pwd2" name="pwd2" type="password">
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <input class="btn btn-success" type="submit" value="Validate">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection

