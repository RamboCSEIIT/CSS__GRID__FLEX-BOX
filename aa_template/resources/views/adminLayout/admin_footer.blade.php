<footer class="footer text-center">
    All Rights Reserved by  {{ env('COMPANY_NAME') }}. Designed and Developed by <a href="{{ env('DEVELOPER_SITE') }}">{{ env('DEVELOPER_NAME') }}</a>.
</footer>
