<!DOCTYPE html>
<html lang="en">

<head>
    <meta -8 charset="utf">
    <meta -UA-Compatible content="IE=edge" http-equiv="X">
    <meta :disabled content="autoRotate" http-equiv="ScreenOrientation">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta name="description" content>
    <meta name="author" content>
    <link type="image" href="02_IMAGES" favicon.png png rel="shortcut icon">
    <title>@yield('title')</title>
    <link href="01_CSS/icon-font.css" rel="stylesheet">
    <style>  @include('01_CSS.za_part_one') </style>
</head>


<body>
@yield('content')
<script type="text/javascript"> @include('00_SCRIPTS.za_part_one')</script>
</body>

</html>