@font-face
{
      font-family: 'Open Sans';
      font-style: normal;
      font-weight: 400;
      src: local("Open Sans Regular"), local("OpenSans-Regular"), url(https://fonts.gstatic.com/s/opensans/v15/mem8YaGs126MiZpBA-UFWJ0bbck.woff2) format("woff2");
      unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
}

@font-face
{
      font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: local("Open Sans Regular"), local("OpenSans-Regular"), url(https://fonts.gstatic.com/s/opensans/v15/mem8YaGs126MiZpBA-UFUZ0bbck.woff2) format("woff2");
  unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
}

@font-face
{
      font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: local("Open Sans Regular"), local("OpenSans-Regular"), url(https://fonts.gstatic.com/s/opensans/v15/mem8YaGs126MiZpBA-UFWZ0bbck.woff2) format("woff2");
  unicode-range: U+1F00-1FFF;
}

@font-face
{
      font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: local("Open Sans Regular"), local("OpenSans-Regular"), url(https://fonts.gstatic.com/s/opensans/v15/mem8YaGs126MiZpBA-UFVp0bbck.woff2) format("woff2");
  unicode-range: U+0370-03FF;
}

@font-face
{
      font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: local("Open Sans Regular"), local("OpenSans-Regular"), url(https://fonts.gstatic.com/s/opensans/v15/mem8YaGs126MiZpBA-UFWp0bbck.woff2) format("woff2");
  unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB;
}

@font-face
{
      font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: local("Open Sans Regular"), local("OpenSans-Regular"), url(https://fonts.gstatic.com/s/opensans/v15/mem8YaGs126MiZpBA-UFW50bbck.woff2) format("woff2");
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
}

@font-face
{
      font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: local("Open Sans Regular"), local("OpenSans-Regular"), url(https://fonts.gstatic.com/s/opensans/v15/mem8YaGs126MiZpBA-UFVZ0b.woff2) format("woff2");
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}

.pp
{
      -webkit-animation: none;
      animation: none;
      border: 0;
      color: #C55;
      display: block;
}

:root
{
      --color-grey-dark-1: #333;
      --color-grey-dark-2: #777;
      --color-grey-dark-3: #999;
      --color-grey-light-1: #faf9f9;
      --color-grey-light-2: #f4f2f2;
      --color-grey-light-3: #f0eeee;
      --color-grey-light-4: #ccc;
      --color-primary: #eb2f64;
      --color-primary-dark: #BA265D;
      --color-primary-light: #FF3366;
      --line: 1px solid var(--color-grey-light-2);
      --shadow-dark: 0 2rem 6rem rgba(0,0,0,.3);
      --shadow-light: 0 2rem 5rem rgba(0,0,0,.06);
}

*
{
      margin: 0;
      padding: 0;
}

html
{
      -webkit-box-sizing: border-box;
      box-sizing: border-box;
      font-size: 62.5%;
}

body
{
      -webkit-background-size: cover;
      background-repeat: no-repeat;
      background-size: cover;
      color: var(--color-grey-dark-2);
      font-family: 'Open Sans', sans-serif;
      font-weight: 400;
      line-height: 1.6;
      min-height: 100vh;
}

*,
*::before,
*::after
{
      -webkit-box-sizing: inherit;
      box-sizing: inherit;
}

.container
{
      -webkit-box-shadow: var(--shadow-dark);
      background-color: var(--color-grey-light-2);
      box-shadow: var(--shadow-dark);
      margin: 8rem auto;
      max-width: 120rem;
      min-height: 50rem;
}

.header
{
      -ms-flex-align: center;
      -ms-flex-pack: justify;
      -webkit-align-items: center;
      -webkit-box-align: center;
      -webkit-box-pack: justify;
      -webkit-justify-content: space-between;
      align-items: center;
      background-color: #fff;
      border-bottom: var(--line);
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      font-size: 1.4rem;
      height: 7rem;
      justify-content: space-between;
}

.content
{
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
}

.sidebar
{
      -ms-flex: 0 0 18%;
      -webkit-box-flex: 0;
      -webkit-flex: 0 0 18%;
      background-color: var(--color-grey-dark-1);
      flex: 0 0 18%;
}

.hotel-view
{
      -ms-flex: 1;
      -webkit-box-flex: 1;
      -webkit-flex: 1;
      background-color: #fff;
      background-color: orangered;
      flex: 1;
}

.detail
{
}

.description
{
}

.user-reviews
{
}

.logo
{
      height: 3.25rem;
      margin-left: 2rem;
}

.search
{
      -ms-flex: 0 0 40%;
      -ms-flex-align: center;
      -ms-flex-pack: center;
      -webkit-align-items: center;
      -webkit-box-align: center;
      -webkit-box-flex: 0;
      -webkit-box-pack: center;
      -webkit-flex: 0 0 40%;
      -webkit-justify-content: center;
      align-items: center;
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      flex: 0 0 40%;
      justify-content: center;
}

.search__input
{
      -o-transition: all .2s;
      -webkit-border-radius: 100px;
      -webkit-transition: all .2s;
      background-color: var(--color-grey-light-2);
      border: none;
      border-radius: 100px;
      color: inherit;
      font-family: inherit;
      font-size: inherit;
      margin-right: -3.25rem;
      padding: .7rem 2rem;
      transition: all .2s;
      width: 90%;
}

.search__input:focus
{
      background-color: var(--color-grey-light-3);
      outline: none;
      width: 100%;
}

.search__input::-webkit-input-placeholder
{
      color: var(--color-grey-light-4);
      font-weight: 100;
}

.search__input:focus + .search__button
{
      background-color: var(--color-grey-light-3);
}

.search__button
{
      background-color: var(--color-grey-light-2);
      border: none;
}

.search__button:focus
{
      outline: none;
}

.search__button:active
{
      -ms-transform: translateY(2px);
      -webkit-transform: translateY(2px);
      transform: translateY(2px);
}

.search__icon
{
      fill: var(--color-grey-dark-3);
      height: 2rem;
      width: 2rem;
}

.user-nav
{
      -ms-flex-align: center;
      -ms-flex-item-align: stretch;
      -webkit-align-items: center;
      -webkit-align-self: stretch;
      -webkit-box-align: center;
      align-items: center;
      align-self: stretch;
      background-color: greenyellow;
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
}

.user-nav > *
{
      -ms-flex-align: center;
      -webkit-align-items: center;
      -webkit-box-align: center;
      align-items: center;
      cursor: pointer;
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      height: 100%;
      padding: 0 2rem;
}

.user-nav > *:hover
{
      background-color: var(--color-grey-light-2);
}

.user-nav__icon-box
{
      position: relative;
}

.user-nav__icon
{
      fill: var(--color-grey-dark-2);
      height: 2.25rem;
      width: 2.25rem;
}

.user-nav__notification
{
      -ms-flex-align: center;
      -ms-flex-pack: center;
      -webkit-align-items: center;
      -webkit-border-radius: 50%;
      -webkit-box-align: center;
      -webkit-box-pack: center;
      -webkit-justify-content: center;
      align-items: center;
      background-color: var(--color-primary);
      border-radius: 50%;
      color: #fff;
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      font-size: .8rem;
      height: 1.75rem;
      justify-content: center;
      position: absolute;
      right: 1.1rem;
      top: 1.5rem;
      width: 1.75rem;
}

.user-nav__user-photo
{
      -webkit-border-radius: 50%;
      border-radius: 50%;
      height: 3.75rem;
      margin-right: 1rem;
}

.side-nav
{
}

.legal
{
}

.gallery
{
}

.overview
{
}

.btn-inline
{
}

.paragraph:not(:last-of-type)
{
}

.list
{
}

.recommend
{
}

.review
{
}

.cta
{
}

.btn
{
}
