function fn()
{
    // "On document ready" commands:
    console.log(document.readyState);

   // WebWorker_Start();

  //  getPosts();
    /*
    createPost({title : 'Post Three',body: 'This is Post Three'},getPosts);*/
/*
    createPost({title : 'Post Three',body: 'This is Post Three'})
        .then(getPosts)
        .catch(function (error) {
            console.log(error);
        });
*/

async function init ()
{
       await  createPost({title : 'Post Three',body: 'This is Post Three'});
       getPosts();
}

    init();
}


if(document.readyState != 'loading')
{
    fn();


}
else
{
    document.addEventListener('DOMContentLoaded', fn);
}



/*https://developer.mozilla.org/en-US/docs/Web/API/Document/readyState */