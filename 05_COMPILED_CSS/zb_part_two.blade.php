/* cyrillic-ext */
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: local("Open Sans Regular"), local("OpenSans-Regular"), url(https://fonts.gstatic.com/s/opensans/v15/mem8YaGs126MiZpBA-UFWJ0bbck.woff2) format("woff2");
  unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F; }

/* cyrillic */
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: local("Open Sans Regular"), local("OpenSans-Regular"), url(https://fonts.gstatic.com/s/opensans/v15/mem8YaGs126MiZpBA-UFUZ0bbck.woff2) format("woff2");
  unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116; }

/* greek-ext */
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: local("Open Sans Regular"), local("OpenSans-Regular"), url(https://fonts.gstatic.com/s/opensans/v15/mem8YaGs126MiZpBA-UFWZ0bbck.woff2) format("woff2");
  unicode-range: U+1F00-1FFF; }

/* greek */
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: local("Open Sans Regular"), local("OpenSans-Regular"), url(https://fonts.gstatic.com/s/opensans/v15/mem8YaGs126MiZpBA-UFVp0bbck.woff2) format("woff2");
  unicode-range: U+0370-03FF; }

/* vietnamese */
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: local("Open Sans Regular"), local("OpenSans-Regular"), url(https://fonts.gstatic.com/s/opensans/v15/mem8YaGs126MiZpBA-UFWp0bbck.woff2) format("woff2");
  unicode-range: U+0102-0103, U+0110-0111, U+1EA0-1EF9, U+20AB; }

/* latin-ext */
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: local("Open Sans Regular"), local("OpenSans-Regular"), url(https://fonts.gstatic.com/s/opensans/v15/mem8YaGs126MiZpBA-UFW50bbck.woff2) format("woff2");
  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF; }

/* latin */
@font-face {
  font-family: 'Open Sans';
  font-style: normal;
  font-weight: 400;
  src: local("Open Sans Regular"), local("OpenSans-Regular"), url(https://fonts.gstatic.com/s/opensans/v15/mem8YaGs126MiZpBA-UFVZ0b.woff2) format("woff2");
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD; }

/* custom css variables and custom properties*/
.pp {
  display: block;
  animation: none;
  color: #C55;
  border: 0; }

:root {
  --color-primary: #eb2f64;
  --color-primary-light: #FF3366;
  --color-primary-dark: #BA265D;
  --color-grey-light-1: #faf9f9;
  --color-grey-light-2: #f4f2f2;
  --color-grey-light-3: #f0eeee;
  --color-grey-light-4: #ccc;
  --color-grey-dark-1: #333;
  --color-grey-dark-2: #777;
  --color-grey-dark-3: #999;
  --shadow-dark: 0 2rem 6rem rgba(0,0,0,.3);
  --shadow-light: 0 2rem 5rem rgba(0,0,0,.06);
  --line: 1px solid var(--color-grey-light-2); }

* {
  margin: 0;
  padding: 0; }

html {
  box-sizing: border-box;
  font-size: 62.5%; }

/*  https://stackoverflow.com/questions/10947541/applying-a-background-to-html-and-or-body*/
body {
  /* if it fails open sans then use sanserif*/
  font-family: 'Open Sans', sans-serif;
  font-weight: 400;
  line-height: 1.6;
  background-size: cover;
  background-repeat: no-repeat;
  min-height: 100vh;
  color: var(--color-grey-dark-2); }

*,
*::before,
*::after {
  box-sizing: inherit; }

.container {
  max-width: 120rem;
  margin: 8rem auto;
  background-color: var(--color-grey-light-2);
  box-shadow: var(--shadow-dark);
  min-height: 50rem;
  /*

  margin: 8rem auto;
  background-color: var(--color-grey-light-1);
  box-shadow: var(--shadow-dark);

  min-height: 50rem;

  @media only screen and (max-width: $bp-largest) {
    margin: 0;
    max-width: 100%;
    width: 100%;
  }*/ }

.header {
  font-size: 1.4rem;
  height: 7rem;
  background-color: #fff;
  border-bottom: var(--line);
  display: flex;
  justify-content: space-between;
  align-items: center;
  /*
  ;






  @media only screen and (max-width: $bp-smallest) {
    flex-wrap: wrap;
    align-content: space-around;
    height: 11rem;
  }*/ }

.content {
  display: flex;
  /*


  @media only screen and (max-width: $bp-medium) {
    flex-direction: column;
  }*/ }

.sidebar {
  background-color: var(--color-grey-dark-1);
  flex: 0 0 18%;
  /*
display: flex;

  flex: 0 0 18%;


  flex-direction: column;
  justify-content: space-between;
  */ }

.hotel-view {
  background-color: #fff;
  background-color: orangered;
  /*grow as much u can*/
  flex: 1;
  /*


  flex: 1;
  */ }

.detail {
  /*
  font-size: 1.4rem;
  display: flex;
  padding: 4.5rem;
  background-color: var(--color-grey-light-1);
  border-bottom: var(--line);

  @media only screen and (max-width: $bp-medium) {
    padding: 3rem;
  }

  @media only screen and (max-width: $bp-small) {
    flex-direction: column;
  }*/ }

.description {
  /*
  background-color: #fff;
  box-shadow: var(--shadow-light);
  padding: 3rem;
  flex: 0 0 60%;
  margin-right: 4.5rem;

  @media only screen and (max-width: $bp-medium) {
    padding: 2rem;
    margin-right: 3rem;
  }

  @media only screen and (max-width: $bp-small) {
    margin-right: 0;
    margin-bottom: 3rem;
  }*/ }

.user-reviews {
  /*
  flex: 1;

  display: flex;
  flex-direction: column;
  align-items: center;*/ }

.logo {
  height: 3.25rem;
  margin-left: 2rem; }

.search {
  flex: 0 0 40%;
  display: flex;
  align-items: center;
  justify-content: center;
  /*




  @media only screen and (max-width: $bp-smallest) {
    order: 1;
    flex: 0 0 100%;
    background-color: var(--color-grey-light-2);
  }
 */ }
  .search__input {
    font-family: inherit;
    font-size: inherit;
    color: inherit;
    background-color: var(--color-grey-light-2);
    border: none;
    padding: .7rem 2rem;
    border-radius: 100px;
    width: 90%;
    transition: all .2s;
    margin-right: -3.25rem; }
    .search__input:focus {
      outline: none;
      width: 100%;
      background-color: var(--color-grey-light-3); }
    .search__input::-webkit-input-placeholder {
      font-weight: 100;
      color: var(--color-grey-light-4); }
  .search__input:focus + .search__button {
    background-color: var(--color-grey-light-3); }
  .search__button {
    border: none;
    background-color: var(--color-grey-light-2); }
    .search__button:focus {
      outline: none; }
    .search__button:active {
      transform: translateY(2px); }
  .search__icon {
    height: 2rem;
    width: 2rem;
    fill: var(--color-grey-dark-3); }

.user-nav {
  background-color: greenyellow;
  display: flex;
  align-items: center;
  align-self: stretch; }
  .user-nav > * {
    padding: 0 2rem;
    cursor: pointer;
    height: 100%;
    display: flex;
    align-items: center; }
  .user-nav > *:hover {
    background-color: var(--color-grey-light-2); }
  .user-nav__icon-box {
    position: relative; }
  .user-nav__icon {
    height: 2.25rem;
    width: 2.25rem;
    fill: var(--color-grey-dark-2); }
  .user-nav__notification {
    font-size: .8rem;
    height: 1.75rem;
    width: 1.75rem;
    border-radius: 50%;
    background-color: var(--color-primary);
    color: #fff;
    position: absolute;
    top: 1.5rem;
    right: 1.1rem;
    display: flex;
    justify-content: center;
    align-items: center; }
  .user-nav__user-photo {
    height: 3.75rem;
    border-radius: 50%;
    margin-right: 1rem; }

.side-nav {
  /*
  font-size: 1.4rem;
  list-style: none;
  margin-top: 3.5rem;

  @media only screen and (max-width: $bp-medium) {
    display: flex;
    margin: 0;
  }

  &__item {
    position: relative;

    &:not(:last-child) {
      margin-bottom: .5rem;

      @media only screen and (max-width: $bp-medium) {
        margin: 0;
      }
    }

    @media only screen and (max-width: $bp-medium) {
      flex: 1;
    }
  }

  &__item::before {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
    width: 3px;
    background-color: var(--color-primary);
    transform: scaleY(0);
    transition: transform .2s,
    width .4s cubic-bezier(1,0,0,1) .2s,
    background-color .1s;
  }

  &__item:hover::before,
  &__item--active::before {
    transform: scaleY(1);
    width: 100%;
  }

  &__item:active::before {
    background-color: var(--color-primary-light);
  }

  &__link:link,
  &__link:visited {
    color: var(--color-grey-light-1);
    text-decoration: none;
    text-transform: uppercase;
    display: block;
    padding: 1.5rem 3rem;
    position: relative;
    z-index: 10;

    display: flex;
    align-items: center;

    @media only screen and (max-width: $bp-medium) {
      justify-content: center;
      padding: 2rem;
    }

    @media only screen and (max-width: $bp-small) {
      flex-direction: column;
      padding: 1.5rem .5rem;
    }
  }

  &__icon {

    /*
    width: 1.75rem;
    height: 1.75rem;
    margin-right: 2rem;
    fill: currentColor;

    @media only screen and (max-width: $bp-small) {
      margin-right: 0;
      margin-bottom: .7rem;
      width: 1.5rem;
      height: 1.5rem;
    }
  }*/ }

.legal {
  /*
  font-size: 1.2rem;
  color: var(--color-grey-light-4);
  text-align: center;
  padding: 2.5rem;

  @media only screen and (max-width: $bp-medium) {
    display: none;
  }*/ }

.gallery {
  /*
  display: flex;

  &__photo {
    width: 100%;
    display: block;
  }*/ }

.overview {
  /*
  display: flex;
  align-items: center;
  border-bottom: var(--line);

  &__heading {
    font-size: 2.25rem;
    font-weight: 300;
    text-transform: uppercase;
    letter-spacing: 1px;
    padding: 1.5rem 3rem;

    @media only screen and (max-width: $bp-small) {
      font-size: 1.8rem;
      padding: 1.25rem 2rem;
    }
  }

  &__stars {
    margin-right: auto;
    display: flex;
  }

  &__icon-star,
  &__icon-location {
    width: 1.75rem;
    height: 1.75rem;
    fill: var(--color-primary);
  }

  &__location {
    font-size: 1.2rem;
    display: flex;
    vertical-align: center;
  }

  &__icon-location {
    margin-right: .5rem;
  }

  &__rating {
    background-color: var(--color-primary);
    color: #fff;
    margin-left: 3rem;
    padding: 0 2.25rem;
    align-self: stretch;

    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;

    @media only screen and (max-width: $bp-small) {
      padding: 0 1.5rem;
    }
  }

  &__rating-average {
    font-size: 2.25rem;
    font-weight: 300;
    margin-bottom: -3px;

    @media only screen and (max-width: $bp-small) {
      font-size: 1.8rem;
    }
  }

  &__rating-count {
    font-size: .8rem;
    text-transform: uppercase;

    @media only screen and (max-width: $bp-small) {
      font-size: .5rem;
    }
  }*/ }

.btn-inline {
  /*
  border: none;
  color: var(--color-primary);
  font-size: inherit;
  border-bottom: 1px solid currentColor;
  padding-bottom: 2px;
  display: inline-block;
  background-color: transparent;
  cursor: pointer;
  transition: all .2s;

  & span {
    margin-left: 3px;
    transition: margin-left .2s;
  }

  &:hover {
    color: var(--color-grey-dark-1);

    span {
      margin-left: 8px;
    }
  }

  &:focus {
    outline: none;
    animation: pulsate 1s infinite;
  }
}

@keyframes pulsate {
  0% {
    transform: scale(1);
    box-shadow: none;
  }

  50% {
    transform: scale(1.05);
    box-shadow: 0 1rem 4rem rgba(0,0,0,.25);
  }

  100% {
    transform: scale(1);
    box-shadow: none;
  }*/ }

.paragraph:not(:last-of-type) {
  /*
  margin-bottom: 2rem;*/ }

.list {
  /*
  list-style: none;
  margin: 3rem 0;
  padding: 3rem 0;
  border-top: var(--line);
  border-bottom: var(--line);

  display: flex;
  flex-wrap: wrap;

  &__item {
    flex: 0 0 50%;
    margin-bottom: .7rem;
  }

  &__item::before {
    content: "";
    display: inline-block;
    height: 1rem;
    width: 1rem;
    margin-right: .7rem;

    // Older browsers
    background-image: url(../img/chevron-thin-right.svg);
    background-size: cover;

    //Newer browsers - masks
    @supports (-webkit-mask-image: url()) or (mask-image: url()) {
      background-color: var(--color-primary);
      -webkit-mask-image: url(../img/chevron-thin-right.svg);
      -webkit-mask-size: cover;
      mask-image: url(../img/chevron-thin-right.svg);
      mask-size: cover;
      background-image: none;
    }

  }*/ }

.recommend {
  /*
  font-size: 1.3rem;
  color: var(--color-grey-dark-3);

  display: flex;
  align-items: center;

  &__count {
    margin-right: auto;
  }

  &__friends {
    display: flex;
  }

  &__photo {
    box-sizing: content-box;
    height: 4rem;
    width: 4rem;
    border-radius: 50%;
    border: 3px solid #fff;

    &:not(:last-child) {
      margin-right: -2rem;
    }
  }*/ }

.review {
  /*
  background-color: #fff;
  box-shadow: var(--shadow-light);
  padding: 3rem;
  margin-bottom: 3.5rem;
  position: relative;
  overflow: hidden;

  @media only screen and (max-width: $bp-medium) {
    padding: 2rem;
    margin-bottom: 3rem;
  }

  &__text {
    margin-bottom: 2rem;
    z-index: 10;
    position: relative;
  }

  &__user {
    display: flex;
    align-items: center;
  }

  &__photo {
    height: 4.5rem;
    width: 4.5rem;
    border-radius: 50%;
    margin-right: 1.5rem;
  }

  &__user-box {
    margin-right: auto;
  }

  &__user-name {
    font-size: 1.1rem;
    font-weight: 600;
    text-transform: uppercase;
    margin-bottom: .4rem;
  }

  &__user-date {
    font-size: 1rem;
    color: var(--color-grey-dark-3);
  }

  &__rating {
    color: var(--color-primary);
    font-size: 2.2rem;
    font-weight: 300;
  }

  &::before {
    content: "\201C";
    position: absolute;
    top: -2.75rem;
    left: -1rem;
    line-height: 1;
    font-size: 20rem;
    color: var(--color-grey-light-2);
    font-family: sans-serif;
    z-index: 1;
  }*/ }

.cta {
  /*
  padding: 3.5rem 0;
  text-align: center;

  @media only screen and (max-width: $bp-medium) {
    padding: 2.5rem 0;
  }

  &__book-now {
    font-size: 2rem;
    font-weight: 300;
    text-transform: uppercase;
    margin-bottom: 2.5rem;
  }*/ }

.btn {
  /*
  font-size: 1.5rem;
  font-weight: 300;
  text-transform: uppercase;
  border-radius: 100px;
  border: none;
  background-image: linear-gradient(to right, var(--color-primary-light), var(--color-primary-dark));
  color: #fff;
  position: relative;
  overflow: hidden;
  cursor: pointer;

  & > * {
    display: inline-block;
    height: 100%;
    width: 100%;
    transition: all .2s;
  }

  &__visible {
    padding: 2rem 7.5rem;
  }

  &__invisible {
    position: absolute;
    padding: 2rem 0;
    left: 0;
    top: -100%;
  }

  &:hover {
    background-image: linear-gradient(to left, var(--color-primary-light), var(--color-primary-dark));
  }

  &:hover &__visible {
    transform: translateY(100%);
  }

  &:hover &__invisible {
    top: 0;
  }

  &:focus {
    outline: none;
    animation: pulsate 1s infinite;
  }*/ }
